import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharactersRoutingModule } from './characters-routing.module';
import { CharactersComponent } from './characters.component';
import { CharactersGalleryComponent } from './characters-gallery/characters-gallery.component';
import { CharactersDetailsComponent } from './characters-details/characters-details.component';


@NgModule({
  declarations: [
    CharactersComponent,
    CharactersGalleryComponent,
    CharactersDetailsComponent
  ],
  imports: [
    CommonModule,
    CharactersRoutingModule
  ]
})
export class CharactersModule { }
