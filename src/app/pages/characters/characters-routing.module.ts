import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharactersDetailsComponent } from './characters-details/characters-details.component';
import { CharactersComponent } from './characters.component';

const routes: Routes = [
  {path:"", component:CharactersComponent},
  {path:":id", component:CharactersDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CharactersRoutingModule { }
