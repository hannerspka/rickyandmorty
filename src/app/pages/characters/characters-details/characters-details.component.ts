import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersService } from 'src/app/shared/services/characters.service'

@Component({
  selector: 'app-characters-details',
  templateUrl: './characters-details.component.html',
  styleUrls: ['./characters-details.component.scss']
})
export class CharactersDetailsComponent implements OnInit {

  characterDetail:any = {}
  constructor(private route:ActivatedRoute, private charactersService:CharactersService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((param)=>{
      let idCharacter = param.get('id')
      this.charactersService.getCharactersById(idCharacter).subscribe((character)=> {
        this.characterDetail = character
      

      })
    })
  }

}
