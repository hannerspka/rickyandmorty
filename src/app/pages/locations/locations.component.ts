import { Component, OnInit } from '@angular/core';
import { LocationsService } from 'src/app/shared/services/locations.service';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {


  locationsList:any = []
  infoLocation:any = {}

  constructor(private locationsService:LocationsService) { }

  ngOnInit(): void {
    this.locationsService.getLocations().subscribe( (location) => {
      this.locationsList = location.results
      this.infoLocation = location.info
      console.log(this.locationsList)
      console.log(this.infoLocation)
    })
 

  }

}
