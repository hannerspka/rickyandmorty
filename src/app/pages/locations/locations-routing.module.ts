import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LocationsDetailsComponent } from './locations-details/locations-details.component';
import { LocationsComponent } from './locations.component';

const routes: Routes = [
  {path:"", component:LocationsComponent},
  {path:":id", component:LocationsDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationsRoutingModule { }
