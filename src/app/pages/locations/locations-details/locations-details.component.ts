import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocationsService } from 'src/app/shared/services/locations.service';

@Component({
  selector: 'app-locations-details',
  templateUrl: './locations-details.component.html',
  styleUrls: ['./locations-details.component.scss']
})
export class LocationsDetailsComponent implements OnInit {

  locationDetail:any = {}
  idLocation:any = {}
  residentsLocation:any = []

  constructor(private route:ActivatedRoute, private locationsService:LocationsService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((param) => {
      let idLocation = param.get('id')
      this.locationsService.getLocationById(idLocation).subscribe((location) => {
        this.locationDetail = location
        console.log(this.locationDetail)

        for (let i = 0; i < location.residents.length; i++) {
          const urlResident = location.residents[i];
          this.locationsService.getCharactersByUrl(urlResident).subscribe((resident) => {
            this.residentsLocation.push(resident)
            console.log(this.residentsLocation)

          })
          
        }

      })

    })
  }

}
