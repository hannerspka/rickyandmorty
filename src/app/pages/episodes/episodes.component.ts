import { Component, OnInit } from '@angular/core';
import { EpisodesService } from 'src/app/shared/services/episodes.service';

@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.scss']
})
export class EpisodesComponent implements OnInit {

  episodeList:any = []

  constructor(private espisodesService:EpisodesService) { }

  ngOnInit(): void {
    this.espisodesService.getEpisodes().subscribe((episode)=> {
      this.episodeList = episode.results
      console.log(this.episodeList)
    })
  }

}
